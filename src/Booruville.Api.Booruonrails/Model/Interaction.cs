﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BooruonrailsAPI.Responses
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Interaction
    {
        [JsonProperty("id")]
        public int Id { get; private set; }

        [JsonProperty("interaction_type")]
        public string InteractionType { get; private set; }

        [JsonProperty("value")]
        public string Value { get; private set; }

        [JsonProperty("user_id")]
        public int UserId { get; private set; }

        [JsonProperty("image_id")]
        public int ImageId { get; private set; }
    }
}
