﻿using BooruonrailsAPI.Enums;
using BooruonrailsAPI.Responses;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BooruonrailsAPI
{
    public class Client
    {
        private Uri baseUrl;
        public Uri BaseUrl
        {
            get { return baseUrl; }
            set { baseUrl = value ?? throw new ArgumentNullException("BaseUrl"); }
        }

        private HttpClient httpClient;

        public Client(Uri url)
        {
            BaseUrl = url;
            httpClient = new HttpClient();
        }

        public async Task<Rating> CallInteractionMethod(InteractionMethod method, params string[] query)
        {
            var response = await httpClient.PutAsync(GetUri(method, query), new StringContent(""));
            if (!response.IsSuccessStatusCode)
                throw new HttpRequestException($"Interaction call return a {response.StatusCode} code with message: \"{await response.Content.ReadAsStringAsync()}\"");
            var body = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Rating>(body);
        }

        public async Task<Image> GetImage(int id, params string[] query)
        {
            return JsonConvert.DeserializeObject<Image>(await httpClient.GetStringAsync(GetUri(id + ".json", query)));
        }

        public async Task<Image> GetImages(int id, params string[] query)
        {
            return JsonConvert.DeserializeObject<Image>(await httpClient.GetStringAsync(GetUri(id + ".json", query)));
        }

        public async Task<IEnumerable<Image>> GetImages(SearchMethod method, params string[] query)
        {
            List<Image> result = new List<Image>();
            List<Interaction> interactions = new List<Interaction>();

            var uri = GetUri(method, query);
            string json = await httpClient.GetStringAsync(uri);
            
            //TODO: Legacy. Refactoring needed
            JObject js = JObject.Parse(json);
            foreach (JProperty w in js.Children())
            {
                if (w.Name == "search" || w.Name == "images")
                    foreach (JArray x in w.Children())
                        foreach (JObject m in x.Children<JObject>())
                            result.Add(JsonConvert.DeserializeObject<Image>(m.ToString()));
                if (w.Name == "interactions")
                    foreach (JArray x in w.Children())
                        foreach (JObject m in x.Children<JObject>())
                            interactions.Add(JsonConvert.DeserializeObject<Interaction>(m.ToString()));
            }

            for (int i = 0; i < interactions.Count; i++)
                for (int j = 0; j < result.Count; j++)
                    if (interactions[i].ImageId == Convert.ToInt32(result[j].Id))
                    {
                        List<Interaction> temp = new List<Interaction>();
                        if (result[j].Interactions != null)
                            temp = result[j].Interactions.ToList();
                        temp.Add(interactions[i]);
                        result[j].Interactions = temp.ToArray();
                    }

            return result;
        }

        protected Uri GetUri(string Path, params string[] query)
        {
            return new Uri(BaseUrl, Path + GetQuery(query));
        }

        protected virtual Uri GetUri(InteractionMethod method, params string[] query)
        {
            string queryRow = GetQuery(query);
            switch (method)
            {
                case InteractionMethod.Vote:
                    return new Uri(BaseUrl, "/api/v2/interactions/vote.json" + queryRow);
                case InteractionMethod.Favourites:
                default:
                    return new Uri(BaseUrl, "/api/v2/interactions/fave.json" + queryRow);
            }
        }

        protected virtual Uri GetUri(SearchMethod method, params string[] query)
        {
            string queryRow = GetQuery(query);
            switch (method)
            {
                case SearchMethod.AllTop:
                    var allTimeTopQuery = queryRow.Length == 0 ? "?q=%2A&sd=desc&sf=score" : "&q=%2A&sd=desc&sf=score";
                    return new Uri(BaseUrl, "/search.json" + queryRow + allTimeTopQuery);
                case SearchMethod.Top:
                    var recentTop = queryRow.Length == 0 ? "?q=first_seen_at.gt%3A3+days+ago&sd=desc&sf=score" : "&q=first_seen_at.gt%3A3+days+ago&sd=desc&sf=score";
                    return new Uri(BaseUrl, "/search.json" + queryRow + recentTop);
                case SearchMethod.WatchList:
                    var watchQuery = queryRow.Length == 0 ? "?q=my:watched" : "&q=my:watched";
                    return new Uri(BaseUrl, "/search.json" + queryRow + watchQuery);
                case SearchMethod.Favourites:
                    var favsQuery = queryRow.Length == 0 ? "?q=my:faves" : "&q=my:faves";
                    return new Uri(BaseUrl, "/search.json" + queryRow + favsQuery);
                case SearchMethod.Images:
                    return new Uri(BaseUrl, "/images.json" + queryRow);
                case SearchMethod.Search:
                default:
                    return new Uri(BaseUrl, "/search.json" + queryRow);
            }
        }

        protected virtual Uri GetUri(TagsType type, params string[] query)
        {
            string queryRow = GetQuery(query);
            switch (type)
            {
                case TagsType.Aliases:
                    return new Uri(BaseUrl, "/tags/aliases.json" + queryRow);
                case TagsType.Implied:
                    return new Uri(BaseUrl, "/tags/implied.json" + queryRow);
                case TagsType.All:
                default:
                    return new Uri(BaseUrl, "/tags.json" + queryRow);
            }
        }

        protected string GetQuery(params string[] query)
        {
            if (query.Length == 0)
                return string.Empty;
            string queryRow = "?";
            for (int i = 0; i < query.Length; i++)
                if (i + 1 == query.Length)
                    queryRow += query[i];
                else
                    queryRow += query[i] + "&";
            return Uri.EscapeUriString(queryRow);

        }
    }
}
