﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booruville.Model
{
    public class Image
    {
        public ulong Id { get; set; }        
        public ulong ProviderId { get; set; }
        public string LocalIdentifier { get; set; }
        public string UrlPreview { get; set; }
        public string UrlOptimized { get; set; }
        public string UrlFull { get; set; }

        public Provider Provider { get; set; }
        public List<ImageTag> Tags { get; set; }
    }
}
