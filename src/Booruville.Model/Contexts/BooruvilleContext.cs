﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booruville.Model.Contexts
{
    public class BooruvilleContext : DbContext
    {
        public DbSet<Image> Images { get; set; }
        public DbSet<ImageTag> ImageTags { get; set; }
        public DbSet<ImageRequest> ImageRequests { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Provider> Providers { get; set; }
        public DbSet<SocialNetwork> SocialNetworks { get; set; }
        public DbSet<Tag> Tags { get; set; }

        public BooruvilleContext(DbContextOptions<BooruvilleContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Relationships
            #region Images
            modelBuilder.Entity<Image>()
                .HasOne(x => x.Provider)
                .WithMany(x => x.Images)
                .HasForeignKey(x => x.ProviderId);
            modelBuilder.Entity<Image>()
                .HasIndex(x => x.LocalIdentifier);
            modelBuilder.Entity<Image>()
                .HasIndex(x => x.ProviderId);

            #endregion

            #region Image requests 
            modelBuilder.Entity<ImageRequest>()
                .HasOne(x => x.ResponseImage)
                .WithMany()
                .HasForeignKey(x => x.ResponseImageId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<ImageRequest>()
                .HasOne(x => x.ResponseImageProvider)
                .WithMany()
                .HasForeignKey(x => x.ResponseImageProviderId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<ImageRequest>()
                .HasOne(x => x.User)
                .WithMany(x => x.Requests)
                .HasForeignKey(x => x.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
            #endregion

            #region Provider
            modelBuilder.Entity<Provider>()
                .HasMany(x => x.Images)
                .WithOne(x => x.Provider);

            modelBuilder.Entity<Provider>().HasData(new Provider() { Id = 1, Name = "derpibooru", Url = "https://derpibooru.org/" });
            #endregion

            #region Social Network
            modelBuilder.Entity<SocialNetwork>()
                .HasMany(x => x.Users)
                .WithOne(x => x.SocialNetwork)
                .HasForeignKey(x => x.SocialNetworkId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<SocialNetwork>()
                .HasIndex(x => x.Name)
                .IsUnique();

            modelBuilder.Entity<SocialNetwork>().HasData(new SocialNetwork() { Id = 1, Name = "telegram", Url = "https://telegram.org/" });
            #endregion

            #region User
            modelBuilder.Entity<User>()
                .HasMany(x => x.Requests)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId);
            modelBuilder.Entity<User>()
                .HasOne(x => x.SocialNetwork)
                .WithMany(x => x.Users)
                .HasForeignKey(x => x.SocialNetworkId);
            #endregion

            #region ImageTag
            //Many to many relationship
            modelBuilder.Entity<ImageTag>()
                .HasKey(t => new { t.ImageId, t.TagId });

            modelBuilder.Entity<ImageTag>()
                .HasOne(x => x.Image)
                .WithMany(x => x.Tags)
                .HasForeignKey(x => x.ImageId);
            modelBuilder.Entity<ImageTag>()
                .HasOne(x => x.Tag)
                .WithMany(x => x.Images)
                .HasForeignKey(x => x.TagId);
            #endregion

            #region Tag
            modelBuilder.Entity<Tag>()
                .HasIndex(x => x.Name)
                .IsUnique();
            #endregion
            #endregion
        }

        //TODO: Replace on extension method?
        public SocialNetwork AddSocialNetworkIfNotExists(string name, string url)
        {
            using (var transaction = Database.BeginTransaction())
            {
                name = name.ToLower();
                var socialNetwork = SocialNetworks.Where(x => name == x.Name).SingleOrDefault();

                if (socialNetwork == default(SocialNetwork))
                {
                    socialNetwork = new SocialNetwork() { Name = name, Url = url };
                    SocialNetworks.Add(socialNetwork);
                    SaveChanges();
                }
                transaction.Commit();
                return socialNetwork;
            }
        }

        public User AddUserIfNotExists(string socialNetworkName, string localIdentifier, string nickname)
        {
            using (var transaction = Database.BeginTransaction())
            {
                socialNetworkName = socialNetworkName.ToLower();
                var user = Users.Include(x => x.SocialNetwork)
                    .Where(x => x.LocalIdentifier == localIdentifier && x.SocialNetwork.Name == socialNetworkName)
                    .AsNoTracking()
                    .SingleOrDefault();

                if (user == default(User))
                {
                    var socialNetwork = SocialNetworks
                        .Include(x => x.Users)
                        .Where(x => x.Name == socialNetworkName)
                        .First();

                    user = new User() { SocialNetwork = socialNetwork, Nickname = nickname, LocalIdentifier = localIdentifier };
                    socialNetwork.Users.Add(user);
                    SaveChanges();
                }
                transaction.Commit();
                return user;
            }
        }
    }
}
