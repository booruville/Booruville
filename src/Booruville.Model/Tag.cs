﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booruville.Model
{
    public class Tag
    {
        public ulong Id { get; set; }
        public string Name { get; set; }

        public List<ImageTag> Images { get; set; }
    }
}
