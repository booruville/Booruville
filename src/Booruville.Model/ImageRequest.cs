﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booruville.Model
{
    public class ImageRequest
    {
        public ulong Id { get; set; }
        public DateTime Date { get; set; }
        public ulong UserId { get; set; }        
        public string Message { get; set; }
        public ulong? ResponseImageProviderId { get; set; }
        public ulong? ResponseImageId { get; set; }

        public User User { get; set; }
        public Provider ResponseImageProvider { get; set; }
        public Image ResponseImage { get; set; }
    }
}
