﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booruville.Model
{
    public class ImageTag
    {
        public Image Image { get; set; }
        public ulong ImageId { get; set; }

        public Tag Tag { get; set; }
        public ulong TagId { get; set; }
    }
}
