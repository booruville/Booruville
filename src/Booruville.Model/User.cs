﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booruville.Model
{
    public class User
    {
        public ulong Id { get; set; }
        public ulong SocialNetworkId { get; set; }
        public string LocalIdentifier { get; set; }
        public string Nickname { get; set; }

        public SocialNetwork SocialNetwork { get; set; }
        public List<ImageRequest> Requests { get; set; }
    }
}
