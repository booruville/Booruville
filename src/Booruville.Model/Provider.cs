﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booruville.Model
{
    public class Provider
    {
        public ulong Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }

        public List<Image> Images { get; set; }
    }
}
