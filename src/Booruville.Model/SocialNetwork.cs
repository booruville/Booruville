﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booruville.Model
{
    public class SocialNetwork
    {
        public ulong Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }

        public List<User> Users { get; set; } 
    }
}
