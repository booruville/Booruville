﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BooruonrailsAPI.Responses;
using Booruville.Services;
using Booruville.Services.Interfaces;
using Booruville.Services.Telegram;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot.Types;
using Telegram.Bot.Types.InputFiles;
using Microsoft.Extensions.Logging;
using Telegram.Bot.Types.Enums;
using Booruville.Model.Contexts;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Booruville.Controllers
{
    [Route("telegram")]
    public class TelegramController : Controller
    {
        private readonly IOptions<Settings> _telegramSettings;
        private readonly TelegramService _telegramService;
        private readonly ILogger<TelegramController> _logger;
        private readonly BooruvilleContext _booruvilleContext;
        private readonly IBooruService _booruService;

        public TelegramController(BooruvilleContext booruvilleContext, TelegramService telegramService, IOptions<Settings> telegramSettings, ILoggerFactory loggerFactory, IBooruService booruService)
        {
            _telegramSettings = telegramSettings;
            _telegramService = telegramService;
            _logger = loggerFactory.CreateLogger<TelegramController>();
            _booruvilleContext = booruvilleContext;
            _booruService = booruService;
        }

        [HttpGet]

        public ActionResult Get()
        {
            return Redirect("/");
        }

        // POST api/<controller>
        [HttpPost("{token?}")]
        public async Task<ActionResult> Post(string token, [FromBody]Update update)
        {
            if (update == null)
                return BadRequest();
            if (token == null)
                token = string.Empty;
            if (!_telegramSettings.Value.WebhookToken.Equals(token))
                return StatusCode(403);
            else
            {
                try
                {
                    switch (update.Type)
                    {
                        case UpdateType.CallbackQuery:
                            await _telegramService.Process(update.CallbackQuery, _booruvilleContext, _booruService);
                            break;
                        case UpdateType.InlineQuery:
                            await _telegramService.Process(update.InlineQuery, _booruvilleContext, _booruService);
                            break;
                        case UpdateType.ChosenInlineResult:
                            await _telegramService.Process(update.ChosenInlineResult, _booruvilleContext, _booruService);
                            break;
                        case UpdateType.Message:
                        case UpdateType.EditedMessage:
                            Message message = update.Message != null ? update.Message : update.EditedMessage;
                            if (message.Chat.Type == ChatType.Supergroup)
                                await _telegramService.Moderate(message);
                            if (message.Chat.Type == ChatType.Private)
                                await _telegramService.Process(message, _booruvilleContext, _booruService);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogCritical(ex.Message + "\r\n" + ex.StackTrace);
                }
            }

            return NoContent();
        }
    }
}
