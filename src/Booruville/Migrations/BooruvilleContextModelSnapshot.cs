﻿// <auto-generated />
using System;
using Booruville.Model.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Booruville.Migrations
{
    [DbContext(typeof(BooruvilleContext))]
    partial class BooruvilleContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.0-rtm-30799");

            modelBuilder.Entity("Booruville.Model.Image", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LocalIdentifier");

                    b.Property<ulong>("ProviderId");

                    b.Property<string>("UrlFull");

                    b.Property<string>("UrlOptimized");

                    b.Property<string>("UrlPreview");

                    b.HasKey("Id");

                    b.HasIndex("LocalIdentifier");

                    b.HasIndex("ProviderId");

                    b.ToTable("Images");
                });

            modelBuilder.Entity("Booruville.Model.ImageRequest", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Date");

                    b.Property<string>("Message");

                    b.Property<ulong?>("ResponseImageId");

                    b.Property<ulong?>("ResponseImageProviderId");

                    b.Property<ulong>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("ResponseImageId");

                    b.HasIndex("ResponseImageProviderId");

                    b.HasIndex("UserId");

                    b.ToTable("ImageRequests");
                });

            modelBuilder.Entity("Booruville.Model.ImageTag", b =>
                {
                    b.Property<ulong>("ImageId");

                    b.Property<ulong>("TagId");

                    b.HasKey("ImageId", "TagId");

                    b.HasIndex("TagId");

                    b.ToTable("ImageTags");
                });

            modelBuilder.Entity("Booruville.Model.Provider", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<string>("Url");

                    b.HasKey("Id");

                    b.ToTable("Providers");

                    b.HasData(
                        new { Id = 1ul, Name = "derpibooru", Url = "https://derpibooru.org/" }
                    );
                });

            modelBuilder.Entity("Booruville.Model.SocialNetwork", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<string>("Url");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("SocialNetworks");

                    b.HasData(
                        new { Id = 1ul, Name = "telegram", Url = "https://telegram.org/" }
                    );
                });

            modelBuilder.Entity("Booruville.Model.Tag", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("Tags");
                });

            modelBuilder.Entity("Booruville.Model.User", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LocalIdentifier");

                    b.Property<string>("Nickname");

                    b.Property<ulong>("SocialNetworkId");

                    b.HasKey("Id");

                    b.HasIndex("SocialNetworkId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("Booruville.Model.Image", b =>
                {
                    b.HasOne("Booruville.Model.Provider", "Provider")
                        .WithMany("Images")
                        .HasForeignKey("ProviderId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Booruville.Model.ImageRequest", b =>
                {
                    b.HasOne("Booruville.Model.Image", "ResponseImage")
                        .WithMany()
                        .HasForeignKey("ResponseImageId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Booruville.Model.Provider", "ResponseImageProvider")
                        .WithMany()
                        .HasForeignKey("ResponseImageProviderId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("Booruville.Model.User", "User")
                        .WithMany("Requests")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Booruville.Model.ImageTag", b =>
                {
                    b.HasOne("Booruville.Model.Image", "Image")
                        .WithMany("Tags")
                        .HasForeignKey("ImageId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Booruville.Model.Tag", "Tag")
                        .WithMany("Images")
                        .HasForeignKey("TagId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Booruville.Model.User", b =>
                {
                    b.HasOne("Booruville.Model.SocialNetwork", "SocialNetwork")
                        .WithMany("Users")
                        .HasForeignKey("SocialNetworkId")
                        .OnDelete(DeleteBehavior.Restrict);
                });
#pragma warning restore 612, 618
        }
    }
}
