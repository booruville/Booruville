﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Booruville.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using Booruville.Model.Contexts;
using Booruville.Services.Interfaces;

namespace Booruville
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //TODO: Transfer to PostgreSQL
            services.AddDbContext<BooruvilleContext>(options => options.UseSqlite(Configuration.GetConnectionString("BooruvilleConnection"),
                b => b.MigrationsAssembly("Booruville")));

            services.Configure<Services.Telegram.Settings>(Configuration.GetSection("Telegram"));
            services.Configure<Services.Derpibooru.Settings>(Configuration.GetSection("Derpibooru"));
            services.AddSingleton<TelegramService>();
            services.AddScoped<IBooruService, DerpibooruService>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
