﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booruville.Services.Interfaces
{
    public interface IBooruService
    {
        Task<IEnumerable<Model.Image>> Search(string query, int page);
    }
}
