﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booruville.Services.Telegram
{
    public class Command
    {
        public bool ErrorOccurred { get; set; } = false;

        [Option('t', "tags", Required = false, HelpText = "Returns tagged image")]
        public IEnumerable<string> Tags { get; set; }

        [Option('r', "random", Required = false, Default = false, HelpText = "Returns random image")]
        public bool Random { get; set; }

        [Option('q', "query", Required = false, HelpText = "Returns image from custom query request")]
        public string Query { get; set; }

        [Option("top", Required = false, Default = false, HelpText = "Returns image from the top list, if exists")]
        public bool TopRequired { get; set; }


        //TODO: Implement
        [Option('b', "booru", Required = false, HelpText = "Returns image from selected booru", Hidden = true)]
        public bool? SelectedBooru { get; set; }
    }
}
