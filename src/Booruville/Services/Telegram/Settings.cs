﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booruville.Services.Telegram
{
    public class Settings
    {
        public string WebhookToken { get; set; }
        public string BotToken { get; set; }

        public Moderation.Settings Moderation { get; set; }
    }
}
