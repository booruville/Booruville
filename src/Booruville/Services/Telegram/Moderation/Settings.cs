﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booruville.Services.Telegram.Moderation
{
    public class Settings
    {
        public long[] Whitelist { get; set; }
        public long[] Moderators { get; set; }

        public Rule[] Rules { get; set; }
    }
}
