﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booruville.Services.Telegram.Moderation
{
    [Flags]
    public enum Action
    {
        None = 0,
        Allow = 1,
        Notify = 2,
        Report = 4,
        Remove = 8,
        Kick = 16,
        Ban = 32
    }
}
