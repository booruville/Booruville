﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booruville.Services.Telegram.Moderation
{
    public class Rule
    {
        public long ChatId { get; set; }

        public string Type { get; set; }
        public string Value { get; set; }
        public string Comment { get; set; }
        public Action Action { get; set; }

        public string OnReportMessage { get; set; }
        public string OnRemoveMessage { get; set; }
        public string OnKickMessage { get; set; }
        public string OnBanMessage { get; set; }
    }
}
