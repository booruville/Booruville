﻿using Booruville.Model.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InlineQueryResults;
using Telegram.Bot.Types.ReplyMarkups;

namespace Booruville.Services.Telegram.Scenarios
{
    public class UsageScenario : Scenario
    {
        public const string USAGE_FORMAT =
@"This is inline-bot for Telegram which currently uses Derpibooru.

*Warning:* This bot working without filters. That means NSFW images included at search results!

*Examples*
@BooruvilleBot safe
@BooruvilleBot explicit page=3
@BooruvilleBot (Princess Luna AND Princess Celestia) OR Twilight Sparkle page=5

Learn more about Derpibooru
[Site](https://derpibooru.org/) | [Syntax](https://derpibooru.org/search/syntax) | [Our group](https://vk.com/galleryofluna) | [Our channel](https://t.me/Booruville)

If something goes wrong with bot, please mail @Sparin about this. He will take care of it.";

        public UsageScenario(TelegramBotClient botClient) : base(botClient)
        {

        }

        public override async Task Process(Message message, BooruvilleContext booruvilleContext)
        {
            await Telegram.SendTextMessageAsync(message.Chat.Id, USAGE_FORMAT, ParseMode.Markdown, true, false, 0, new ReplyKeyboardRemove());
        }

        public override Task Process(CallbackQuery callbackQuery, BooruvilleContext booruvilleContext)
        {
            throw new NotSupportedException();
        }
    }
}
