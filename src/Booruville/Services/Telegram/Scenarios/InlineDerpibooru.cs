﻿using BooruonrailsAPI;
using BooruonrailsAPI.Enums;
using Booruville.Model.Contexts;
using Booruville.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.InlineQueryResults;

namespace Booruville.Services.Telegram.Scenarios
{
    public class InlineDerpibooru : InlineQueryHandler
    {
        public BooruonrailsAPI.Client client = new Client(new Uri("http://0s.mrsxe4djmjxw64tvfzxxezy.nblz.ru/"));

        public InlineDerpibooru(TelegramBotClient botClient) : base(botClient)
        {

        }

        public override async Task Process(InlineQuery inlineQuery, BooruvilleContext booruvilleContext, IBooruService booruService)
        {
            var match = Regex.Match(inlineQuery.Query, @"page=\d{1,4}");
            int page = match.Success ? Convert.ToInt32(match.Value.Replace("page=", string.Empty)) : 0;
            var images = await booruService.Search(Regex.Replace(inlineQuery.Query, @"page=\d{1,4}", string.Empty), page);

            using (var transaction = booruvilleContext.Database.BeginTransaction())
            {
                var user = booruvilleContext.Users.Include(x => x.Requests)
                .Where(x => x.LocalIdentifier == inlineQuery.From.Id.ToString())
                .First();
                user.Requests.Add(new Model.ImageRequest()
                {
                    Date = DateTime.Now,
                    Message = inlineQuery.Query,
                    ResponseImageProviderId = images.FirstOrDefault()?.ProviderId
                });
                booruvilleContext.SaveChanges();
                transaction.Commit();
            }

            var results = images.Select(x =>
            {
                switch (Path.GetExtension(x.UrlFull))
                {
                    case ".webm":
                    case ".mp4":
                        return (InlineQueryResultBase)new InlineQueryResultMpeg4Gif(x.Id.ToString("X4"), x.UrlFull.Replace(".webm", ".mp4"), x.UrlPreview.Replace(".webm", ".mp4"));
                    case ".gif":
                        return (InlineQueryResultBase)new InlineQueryResultGif(x.Id.ToString("X4"), x.UrlOptimized, x.UrlPreview);
                    default:
                        return (InlineQueryResultBase)new InlineQueryResultPhoto(x.Id.ToString("X4"), x.UrlOptimized, x.UrlPreview);
                }
            }).ToArray();
            await Telegram.AnswerInlineQueryAsync(inlineQuery.Id, results, 1800, isPersonal: false, switchPmText: "Help me!",switchPmParameter:"help");
        }

        public override async Task Process(ChosenInlineResult chosenInlineResult, BooruvilleContext booruvilleContext, IBooruService booruService)
        {
            using (var transaction = booruvilleContext.Database.BeginTransaction())
            {
                var imageRequest = await booruvilleContext.ImageRequests
                    .Include(x => x.User)
                    .ThenInclude(x => x.SocialNetwork)
                    .Where(x => x.User.LocalIdentifier == chosenInlineResult.From.Id.ToString() && chosenInlineResult.Query == x.Message && x.User.SocialNetwork.Name == TelegramService.SOCIAL_NETWORK_NAME && x.ResponseImageId == null)
                    .LastAsync();
                imageRequest.ResponseImageId = Convert.ToUInt64(chosenInlineResult.ResultId, 16);
                await booruvilleContext.SaveChangesAsync();
                transaction.Commit();
            }
        }
    }
}
