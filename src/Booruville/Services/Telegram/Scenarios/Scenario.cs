﻿using Booruville.Model.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace Booruville.Services.Telegram.Scenarios
{
    public abstract class Scenario
    {
        public TelegramBotClient Telegram { get; protected set; }
        public bool IsCompleted { get; set; }

        public Scenario(TelegramBotClient client)
        {
            Telegram = client;
        }

        public virtual Task Process(Message message, BooruvilleContext booruvilleContext)
        {
            throw new NotImplementedException();
        }

        public virtual Task Process(CallbackQuery callbackQuery, BooruvilleContext booruvilleContext)
        {
            throw new NotImplementedException();
        }

    }
}
