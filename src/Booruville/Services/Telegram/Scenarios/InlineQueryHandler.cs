﻿using Booruville.Model.Contexts;
using Booruville.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace Booruville.Services.Telegram.Scenarios
{
    public abstract class InlineQueryHandler
    {
        public TelegramBotClient Telegram { get; protected set; }

        public InlineQueryHandler(TelegramBotClient client)
        {
            Telegram = client;
        }

        public virtual Task Process(InlineQuery inlineQuery, BooruvilleContext booruvilleContext, IBooruService booruService)
        {
            throw new NotImplementedException();
        }

        public virtual Task Process(ChosenInlineResult chosenInlineResult, BooruvilleContext booruvilleContext, IBooruService booruServiced)
        {
            throw new NotImplementedException();
        }
    }
}
