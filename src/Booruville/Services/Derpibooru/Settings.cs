﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booruville.Services.Derpibooru
{
    public class Settings
    {
        public string ApiKey { get; set; }
        public string BaseAddress { get; set; }
    }
}
