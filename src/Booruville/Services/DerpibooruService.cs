﻿using BooruonrailsAPI;
using BooruonrailsAPI.Enums;
using BooruonrailsAPI.Responses;
using Booruville.Model.Contexts;
using Booruville.Services.Derpibooru;
using Booruville.Services.Interfaces;
using Booruville.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booruville.Services
{
    public class DerpibooruService : IBooruService
    {
        public const string PROVIDER_NAME = "derpibooru";
        public const string PROVIDER_URL = "https://derpibooru.org/";

        private readonly Settings _settings;

        private Client client;

        private readonly BooruvilleContext _booruvilleContext;

        public DerpibooruService(BooruvilleContext booruvilleContext, IOptions<Settings> settings)
        {
            _booruvilleContext = booruvilleContext;
            _settings = settings.Value;

            client = new Client(new Uri(_settings.BaseAddress));
        }

        //public Task<IEnumerable<Model.Image>> Search(int page, params string[] tags)
        //{
        //    client.GetImages(SearchMethod.Search, $"page={page}");
        //}

        public async Task<IEnumerable<Model.Image>> Search(string query, int page)
        {
            IEnumerable<Image> booruImages = null;
            if (string.IsNullOrWhiteSpace(query))
                booruImages = await client.GetImages(SearchMethod.Top, $"key={_settings.ApiKey}");
            else
                booruImages = await client.GetImages(SearchMethod.Search, $"page={page}", $"q={query}", "sf=wilson&sd=desc", $"key={_settings.ApiKey}");

            using (var transaction = _booruvilleContext.Database.BeginTransaction())
            {
                var provider = _booruvilleContext.Providers.Include(x => x.Images).Single(x => x.Name == PROVIDER_NAME);

                var booruIds = booruImages.Select(x => x.Id.ToString());
                var existingIds = _booruvilleContext.Images.Where(x => x.ProviderId == provider.Id && booruIds.Contains(x.LocalIdentifier))
                    .Select(x => x.LocalIdentifier)
                    .ToArray();
                var tags = booruImages.Select(x => x.Tags.Split(", "))
                    .SelectMany(x => x)
                    .Distinct();
                var images = booruImages.Select(x =>
                {
                    var image = new Model.Image()
                    {
                        LocalIdentifier = x.Id.ToString(),
                        UrlFull = "http:" + x.Representations.Full,
                        UrlOptimized = "http:" + x.Representations.Large,
                        UrlPreview = "http:" + x.Representations.Thumbnail
                    };
                    return image;
                }).Where(x => !existingIds.Contains(x.LocalIdentifier));

                foreach (var tag in tags)
                    _booruvilleContext.Tags.AddIfNotExists(new Model.Tag() { Name = tag }, x => x.Name == tag);
                provider.Images.AddRange(images);
                await _booruvilleContext.SaveChangesAsync();

                images = _booruvilleContext.Images.Where(x => x.ProviderId == provider.Id && booruIds.Contains(x.LocalIdentifier));
                var dbTags = _booruvilleContext.Tags.Where(x => tags.Contains(x.Name)).ToArray();

                foreach (var image in images)
                {
                    var imageTags = booruImages.Where(x => x.Id.ToString() == image.LocalIdentifier).Single().Tags.Split(", ");
                    foreach (var imageTag in imageTags)
                    {
                        var dbTag = dbTags.Single(x => x.Name == imageTag);
                        _booruvilleContext.ImageTags.AddIfNotExists(new Model.ImageTag() { Image = image, Tag = dbTags.Single(x => x.Name == imageTag) }, x => x.TagId == dbTag.Id && x.ImageId == image.Id);
                    }
                }

                await _booruvilleContext.SaveChangesAsync();
                transaction.Commit();
                return _booruvilleContext.Images.AsNoTracking().Where(x => x.ProviderId == provider.Id && booruIds.Contains(x.LocalIdentifier));
            }
        }

        public Task<IEnumerable<Model.Image>> GetRecentTop()
        {
            throw new NotImplementedException();
        }

        public Task<Model.Image> Random()
        {
            throw new NotImplementedException();
        }
    }
}
