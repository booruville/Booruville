﻿using Booruville.Services.Telegram;
using Booruville.Services.Telegram.Moderation;
using Booruville.Services.Telegram.Scenarios;
using CommandLine;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Booruville.Utils;
using Action = Booruville.Services.Telegram.Moderation.Action;
using Settings = Booruville.Services.Telegram.Settings;
using System.Collections.Concurrent;
using Booruville.Model.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Booruville.Services.Interfaces;

namespace Booruville.Services
{
    public class TelegramService
    {
        internal const string SOCIAL_NETWORK_NAME = "telegram";
        internal const string SOCIAL_NETWORK_URL = "https://telegram.org/";

        public readonly TelegramBotClient Client;

        private readonly Parser _commandParser;
        private readonly ILogger<TelegramService> _logger;
        private readonly ConcurrentDictionary<long, Scenario> _sessions;
        private readonly ConcurrentDictionary<long, InlineQueryHandler> _inlineQueries;
        private readonly Settings _settings;
        private readonly IConfiguration _configuration;
        private readonly DbContextOptions<BooruvilleContext> booruvilleContextOptions;

        public Type DefaultInlineQueryScenario { get; set; } = typeof(InlineDerpibooru);

        public TelegramService(IConfiguration configuration, IOptions<Settings> settings, ILoggerFactory loggerFactory)
        {
            _settings = settings.Value;
            _commandParser = new Parser();
            _logger = loggerFactory.CreateLogger<TelegramService>();
            _sessions = new ConcurrentDictionary<long, Scenario>();
            _inlineQueries = new ConcurrentDictionary<long, InlineQueryHandler>();
            _configuration = configuration;

            var booruvilleContextBuilder = new DbContextOptionsBuilder<BooruvilleContext>();
            booruvilleContextBuilder.UseSqlite(_configuration.GetConnectionString("BooruvilleConnection"));
            booruvilleContextOptions = booruvilleContextBuilder.Options;


            Client = new TelegramBotClient(_settings.BotToken);
        }

        public async Task Process(ChosenInlineResult chosenInlineResult, BooruvilleContext booruvilleContext, IBooruService booruService)
        {
            try
            {
                if (!_inlineQueries.ContainsKey(chosenInlineResult.From.Id))
                {
                    booruvilleContext.AddUserIfNotExists(SOCIAL_NETWORK_NAME, chosenInlineResult.From.Id.ToString(), chosenInlineResult.From.Username);
                    _inlineQueries[chosenInlineResult.From.Id] = (InlineQueryHandler)Activator.CreateInstance(DefaultInlineQueryScenario, Client);
                }
                await _inlineQueries[chosenInlineResult.From.Id].Process(chosenInlineResult, booruvilleContext, booruService);
            }
            catch (ApiRequestException ex)
            {
                _logger.LogWarning($"InlineQuery {chosenInlineResult.InlineMessageId} on message {chosenInlineResult.Query} from {chosenInlineResult.From.Username} (ID #{chosenInlineResult.From.Id}) - Can't be responsed\r\nReason: {ex.Message}\r\n{ex.StackTrace}");
            }
            catch (HttpRequestException ex)
            {
                _logger.LogWarning($"InlineQuery {chosenInlineResult.InlineMessageId} on message {chosenInlineResult.Query} from {chosenInlineResult.From.Username} (ID #{chosenInlineResult.From.Id}) - Can't be responsed\r\nReason: {ex.Message}\r\n{ex.StackTrace}");
            }
            catch (DbUpdateException ex)
            {
                _logger.LogWarning($"InlineQuery {chosenInlineResult.InlineMessageId} on message {chosenInlineResult.Query} from {chosenInlineResult.From.Username} (ID #{chosenInlineResult.From.Id}) - Can't be responsed\r\nReason: {ex.Message}\r\n{ex.StackTrace}");
            }
        }

        public async Task Process(InlineQuery inlineQuery, BooruvilleContext booruvilleContext, IBooruService booruService)
        {
            try
            {
                if (!_inlineQueries.ContainsKey(inlineQuery.From.Id))
                {
                    booruvilleContext.AddUserIfNotExists(SOCIAL_NETWORK_NAME, inlineQuery.From.Id.ToString(), inlineQuery.From.Username);
                    _inlineQueries[inlineQuery.From.Id] = (InlineQueryHandler)Activator.CreateInstance(DefaultInlineQueryScenario, Client);
                }
                await _inlineQueries[inlineQuery.From.Id].Process(inlineQuery, booruvilleContext, booruService);
            }
            catch (ApiRequestException ex)
            {
                _logger.LogWarning($"InlineQuery {inlineQuery.Id} on message {inlineQuery.Query} from {inlineQuery.From.Username} (ID #{inlineQuery.From.Id}) - Can't be responsed\r\nReason: {ex.Message}\r\n{ex.StackTrace}");
            }
            catch (HttpRequestException ex)
            {
                _logger.LogWarning($"InlineQuery {inlineQuery.Id} on message {inlineQuery.Query} from {inlineQuery.From.Username} (ID #{inlineQuery.From.Id}) - Can't be responsed\r\nReason: {ex.Message}\r\n{ex.StackTrace}");
            }
            catch (DbUpdateException ex)
            {
                _logger.LogWarning($"InlineQuery {inlineQuery.Id} on message {inlineQuery.Query} from {inlineQuery.From.Username} (ID #{inlineQuery.From.Id}) - Can't be responsed\r\nReason: {ex.Message}\r\n{ex.StackTrace}");
            }
        }

        public async Task Process(Message message, BooruvilleContext booruvilleContext, IBooruService booruService)
        {
            try
            {                
                if (!_sessions.ContainsKey(message.From.Id) || _sessions[message.From.Id].IsCompleted)
                {
                    booruvilleContext.AddUserIfNotExists(SOCIAL_NETWORK_NAME, message.From.Id.ToString(), message.From.Username);
                    _sessions[message.From.Id] = new UsageScenario(Client);
                }
                    await _sessions[message.From.Id].Process(message, booruvilleContext);
            }
            catch (ApiRequestException ex)
            {
                _logger.LogWarning($"Chat #{message.Chat.Id} message #{message.MessageId} from {message.From.Username} (ID #{message.From.Id}) - Can't be responsed\r\nReason: {ex.Message}\r\n{ex.StackTrace}");
            }
            catch (HttpRequestException ex)
            {
                _logger.LogWarning($"Chat #{message.Chat.Id} message #{message.MessageId} from {message.From.Username} (ID #{message.From.Id}) - Can't be responsed\r\nReason: {ex.Message}\r\n{ex.StackTrace}");
            }
            catch (DbUpdateException ex)
            {
                _logger.LogCritical($"Chat #{message.Chat.Id} message #{message.MessageId} from {message.From.Username} (ID #{message.From.Id}) - Can't be responsed\r\nReason: {ex.Message}\r\n{ex.StackTrace}");
            }
        }

        public async Task Process(CallbackQuery callbackQuery, BooruvilleContext booruvilleContext, IBooruService booruService)
        {
            try
            {
                if (_sessions.ContainsKey(callbackQuery.From.Id) && !_sessions[callbackQuery.From.Id].IsCompleted)
                    await _sessions[callbackQuery.From.Id].Process(callbackQuery, booruvilleContext);
                else
                    await Client.AnswerCallbackQueryAsync(callbackQuery.Id, "Session is expired");
            }
            catch (ApiRequestException ex)
            {
                _logger.LogWarning($"CallbackQuery {callbackQuery.Id} on message {callbackQuery.InlineMessageId} from {callbackQuery.From.Username} (ID #{callbackQuery.From.Id}) - Can't be responsed\r\nReason: {ex.Message}\r\n{ex.StackTrace}");
            }
            catch (HttpRequestException ex)
            {
                _logger.LogWarning($"CallbackQuery {callbackQuery.Id} on message {callbackQuery.InlineMessageId} from {callbackQuery.From.Username} (ID #{callbackQuery.From.Id}) - Can't be responsed\r\nReason: {ex.Message}\r\n{ex.StackTrace}");
            }
            catch (DbUpdateException ex)
            {
                _logger.LogCritical($"CallbackQuery {callbackQuery.Id} on message {callbackQuery.InlineMessageId} from {callbackQuery.From.Username} (ID #{callbackQuery.From.Id}) - Can't be responsed\r\nReason: {ex.Message}\r\n{ex.StackTrace}");
            }
        }

        public async Task Moderate(Message message)
        {
            await Moderate(message, _settings.Moderation);
        }

        //TODO: Add PerformApiMethod with try catches and logging
        public async Task Moderate(Message message, Telegram.Moderation.Settings moderationSettings)
        {
            if (message == null)
                throw new NullReferenceException("Message is missing");
            if (moderationSettings.Whitelist != null && moderationSettings.Whitelist.Contains(message.From.Id))
                return;
            if (moderationSettings.Moderators != null && moderationSettings.Moderators.Contains(message.From.Id))
                return;

            string notifyMessage = string.Empty;
            Rule[] appliedRules = null;
            var actions = GetModerationAction(message, out notifyMessage, out appliedRules, moderationSettings.Rules);
            var onRemoveMessage = string.Join("\r\n", appliedRules.Select(x => x.OnRemoveMessage));
            var onBanMessage = string.Join("\r\n", appliedRules.Select(x => x.OnBanMessage));
            var onKickMessage = string.Join("\r\n", appliedRules.Select(x => x.OnKickMessage));
            var onReportMessage = string.Join("\r\n", appliedRules.Select(x => x.OnReportMessage));
            _logger.LogInformation($"Chat #{message.Chat.Id} message #{message.MessageId} from {message.From.Username} (ID #{message.From.Id}) - {actions}");

            foreach (var action in actions.GetFlags())
                try
                {
                    switch (action)
                    {
                        case Action.Ban:
                            await Client.KickChatMemberAsync(message.Chat.Id, message.From.Id, DateTime.Now.AddYears(1337));
                            _logger.LogInformation($"Chat #{message.Chat.Id} {message.From.Username} (ID #{message.From.Id}) - banned forever");
                            break;
                        case Action.Kick:
                            await Client.KickChatMemberAsync(message.Chat.Id, message.From.Id, DateTime.Now.AddMinutes(5));
                            _logger.LogInformation($"Chat #{message.Chat.Id} {message.From.Username} (ID #{message.From.Id}) - banned for 5 minutes");
                            break;
                        case Action.Report:
                            foreach (var moderatorId in moderationSettings.Moderators)
                            {
                                await Client.ForwardMessageAsync(moderatorId, message.Chat.Id, message.MessageId);
                                await Client.SendTextMessageAsync(moderatorId, notifyMessage);
                            }
                            _logger.LogInformation($"Chat #{message.Chat.Id} message #{message.MessageId} from {message.From.Username} (ID #{message.From.Id}) - reported to moderators");
                            break;
                        case Action.Notify:
                            foreach (var moderatorId in moderationSettings.Moderators)
                                await Client.ForwardMessageAsync(moderatorId, message.Chat.Id, message.MessageId);
                            _logger.LogInformation($"Chat #{message.Chat.Id} message #{message.MessageId} from {message.From.Username} (ID #{message.From.Id}) - notified to moderators");
                            break;
                        case Action.Remove:
                            await Client.DeleteMessageAsync(message.Chat.Id, message.MessageId);
                            if (!string.IsNullOrEmpty(onRemoveMessage) && message.Type != MessageType.ChatMembersAdded && message.Type != MessageType.ChatMemberLeft)
                                await Client.SendTextMessageAsync(message.Chat.Id, onRemoveMessage);
                            _logger.LogInformation($"Chat #{message.Chat.Id} message #{message.MessageId} from {message.From.Username} (ID #{message.From.Id}) - deleted");
                            break;
                    }
                    _logger.LogInformation($"Chat #{message.Chat.Id} message #{message.MessageId} from {message.From.Username} (ID #{message.From.Id}) - Action ({action}) perfomed");
                }
                catch (ApiRequestException ex)
                {
                    _logger.LogWarning($"Chat #{message.Chat.Id} message #{message.MessageId} from {message.From.Username} (ID #{message.From.Id}) - Action ({action}) won't be perfomed\r\nReason: {ex.Message}\r\n{ex.StackTrace}");
                }
                catch (HttpRequestException ex)
                {
                    _logger.LogWarning($"Chat #{message.Chat.Id} message #{message.MessageId} from {message.From.Username} (ID #{message.From.Id}) - Action ({action}) won't be perfomed\r\nReason: {ex.Message}\r\n{ex.StackTrace}");
                }
        }

        public Action GetModerationAction(Message message, out string notifyMessage, out Rule[] appliedRules, params Rule[] rules)
        {
            if (message == null)
                throw new NullReferenceException("Message is missing");
            Action action = Action.None;
            StringBuilder notifyBuilder = new StringBuilder(2048);

            appliedRules = rules.Where(x =>
              {
                  var violation = IsRuleViolation(message, x);
                  if (violation)
                  {
                      action = x.Action | action;
                      notifyBuilder.AppendLine($"Rule violation (\"{x.Type}:{x.Value}\") by {message.From.Username}. {x.Action} will be applied for this user");
                  }
                  return violation;
              }).ToArray();

            notifyMessage = notifyBuilder.ToString();
            return action;
        }

        public bool IsRuleViolation(Message message, Rule rule)
        {
            switch (rule.Type)
            {
                case "entity":
                    if (message.Entities != null)
                        foreach (var entity in message.Entities)
                            if (string.Compare(rule.Value, entity.Type.ToString(), true) == 0)
                                return true;
                    break;
                case "regex":
                    if (Regex.IsMatch(message.Text, rule.Value))
                        return true;
                    break;
                case "memberAdded":
                    if (message.Type == MessageType.ChatMembersAdded)
                        return true;
                    break;
                case "memberLeft":
                    if (message.Type == MessageType.ChatMemberLeft)
                        return true;
                    break;
            }
            return false;
        }

        public Command ParseTelegramCommand(string message)
        {
            var result = new Command();
            message = message.Replace("/", "--");
            var arguments = Regex.Matches(message, "(\"{1}[^\"]*\"{1}|-{1,2}\\w+|\\w+)") // Split to args
                .Select(x => Regex.Replace(x.Value, "(^\"|\"$)", string.Empty)) // Remove " from "string" like 
                .ToArray();

            _commandParser.ParseArguments<Command>(arguments)
                .WithParsed(x => { result = x; })
                .WithNotParsed(x => { result.ErrorOccurred = true; });

            return result;
        }


    }
}
